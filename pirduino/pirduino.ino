#include "HID-Project.h"

bool is_detected;

const int debounce_time = 10;

int pir_sensed = 0;
int mmw_sensed = 0;

void setup() {
  is_detected = false;
  
  // PIR input
  pinMode(A3, INPUT);

  // MM wave input
  pinMode(A4, INPUT);
  
  // Serial.begin(115200);

  Keyboard.begin();
}

void loop() {
  // 1 fps
  delay(1000);

  if(pir_sensed > 0) --pir_sensed;
  if(mmw_sensed > 0) --mmw_sensed;

  // just in case
  if(pir_sensed < 0) pir_sensed = 0;
  if(mmw_sensed < 0) mmw_sensed = 0;

  if (digitalRead(A3) == HIGH) pir_sensed = debounce_time;
  if (digitalRead(A4) == HIGH) mmw_sensed = debounce_time;


  if(is_detected) {
    // Condition for detecting stillness
    if(pir_sensed == 0 && mmw_sensed == 0) {
      is_detected = false;
    }

  } else {
    // Condition for detecting movement

    if (pir_sensed > 0) {
      is_detected = true;
    }
  }

  if(is_detected) {
    // While detected, keep waking / pressing keys on the host
    Keyboard.wakeupHost();
    Keyboard.write(KEY_F13);
  }


}
