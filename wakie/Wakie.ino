#include <Keyboard.h>

#define SENSOR_PIN 10   // Senor state input pin

#define LED_PIN  13  // The RX LED has a defined Arduino pin

static bool sensor_previous_state = false;

void setup()
{
  pinMode(SENSOR_PIN, INPUT);     // PIR sensor pin as input
  pinMode(LED_PIN, OUTPUT);    //  RX LED as an output
  digitalWrite(LED_PIN, HIGH); //  RX LED off

  sensor_previous_state = digitalRead(SENSOR_PIN); // get sensor initial state (can be already true)

  // Give 5s after startup as window to reflash
  delay(5000);
}

void loop()
{
  bool sensor_current_state = digitalRead(SENSOR_PIN);
  
  if ( sensor_previous_state == false && sensor_current_state == true )
  {
    // looping until we detect a rising edge
    // when sensor state is trigged, it takes about 20 sec to recover
    digitalWrite(RX_LED_PIN, LOW);   // set the LED on
    
    USBDevice.wakeupHost();
    Keyboard.press( KEY_CAPS_LOCK );
    Keyboard.release( KEY_CAPS_LOCK );

    delay(1000);                  // wait a bit for the led
  } else {
    digitalWrite(RX_LED_PIN, HIGH);    // set the LED off
  }
  
  sensor_previous_state = sensor_current_state;
}
